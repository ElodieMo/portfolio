import './App.css'
import Moi from "./component/Accueil/Moi.jsx";
import {BrowserRouter, Route, Routes} from "react-router-dom";
import "./App.css";
import Footer from "./component/Footer/Footer.jsx";
import Competences from "./component/Header/Competences.jsx";
import Navbar from "./component/Navbar/Navbar.jsx";
import Realisations from "./component/Realisations/Realisations.jsx";
import Mention from "./component/Mention.jsx";

function App() {
    return (
        <div className="App">
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<Moi/>}/>
                    <Route path="/mention" element={<Mention/>}/>
                </Routes>
            </BrowserRouter>
            <Navbar/>
            <Competences/>
            <Realisations/>
            <Footer/>
        </div>
    );
}

export default App
