import './Navbar.css';

function Navbar() {
    return (

        <>
            <nav className="navbar">
                <div className="navbar_name">Morisan Elodie</div>
                <ul className="navbar_links">
                    <li className="navbar_item">
                        <a href="#Accueil" className="navbar_link">Accueil</a></li>
                    <li className="navbar_item">
                        <a href="#competences" className="navbar_link">Compétences</a></li>
                    <li className="navbar_item">
                        <a href="#rea" className="navbar_link">Réalisations</a></li>
                    <li className="navbar_item">
                        <a href="#contact" className="navbar_link">Contact</a></li>
                </ul>

                <button className="navbar_burger">
                    <span className="burger-bar"></span></button>
            </nav>
        </>
    );
}

export default Navbar;