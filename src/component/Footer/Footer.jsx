import './Footer.css';
import linkedin from '/src/assets/linkedin.png';
import github from '/src/assets/github.png'

function Footer() {
    return (
        <>
            <div className="footer">
                <div className="contact" id="contact"> Me contacter</div>
                <div className="mail">
                    Vous souhaitez me contacter ? Dans ce cas vous pouvez m'envoyer un mail à l'adresse
                    elodie.morisan-pro@hotmail.com et je
                    veillerai à vous répondre dans les plus brefs délais.
                </div>
                <div className="media">
                    <a href="#"></a>
                    <a href="#"></a></div>
                <a href="https://www.linkedin.com/in/elodie-morisan-824356187"> <img src={linkedin} alt="linkedin"/></a>
                <a href="https://gitlab.com/users/ElodieMo/projects"> <img src={github} alt="github"/></a>
                <ul className="mentions">
                    <a href="#">
                        <a href="/mention"><li>Mentions légales</li></a>
                    </a>
                    <li>|</li>
                    <a href="#">
                        <li>Politique de confidentialité</li>
                    </a>
                </ul>
                <ul className="copyright"></ul>
                <li>©Copyright 2023 - Morisan Elodie - Tous droits réservés</li>
            </div>
        </>
    );
}

export default Footer;