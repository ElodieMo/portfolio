import "./Realisations.css"

function Realisations() {
    return (
        <>
            <div className='rea' id="rea">Réalisations</div>
            <div className="projet">
                <div className="projet1">Hunt for games
                    <p>A partir d'une réalisation wireframe/ maquette depuis Figma d'un site de jeux vidéo, création du site internet avec REACT, en recupérant les données depuis une API avec AXIOS et avec la mise en place d'un router avec REACT. </p> <a href="https://gitlab.com/ElodieMo/eval-hunt-for-games">lien git du projet</a></div>
                <div className="projet2">Hbsa Collect
                    <p>Création d'un site internet parti front-end sous REACT/javascript, le projet ayant pour but de faciliter la recolte de bouchons pour un club de handibasket. Nécessitant une connexion avec FIREBASE pour la gestion des rôles. </p><a href="https://gitlab.com/ElodieMo/hbsa-collect-react">lien git du projet Front</a>
                    <p>Réalisation de la partie back-end avec la création d'un CRUD en JAVA avec SPRING et HIBERNATE TEST avec POSTMAN et le modèle MVC</p><a href="https://gitlab.com/ElodieMo/hbsa-collect">lien git du projet Back</a></div>
                <div className="projet3">Adac live
                    <p>Pour une association de musique, création a partir d'une maquette réalisé sur MARVEL APP d'une plateforme d'échanges entre professeurs/parents et professeurs/élèves nécessitant une connexion mise en place depuis LARAVEL/PHP comportant les différents rôles, la modélisation ainsi que la construction d'une base de données sur MYSQL WORKBENCH </p><a href="https://gitlab.com/ElodieMo/adac-live-appli">lien git du projet</a></div>
            </div>
        </>
    );
}

export default Realisations;