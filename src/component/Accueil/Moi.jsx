import "./Moi.css";
import photo from '/src/assets/Image.jpeg'

function Moi() {
    return (
        <>
            <div className="moi" id="Accueil">Accueil</div>
            <div className="fonction">Conceptrice développeuse d'applications</div>


            <div className="container">
                <div className="photo"><img src={photo} alt="photo"/></div>
                <div className="cv">
                <div className="describe">Après avoir travailler durant 5 ans dans une enseigne d'ammeublement et avec
                    un parcours de vie particulier, j'ai eu envie de renouveau, envie de faire un métier de passion,
                    attiré par les métiers de l'informatique depuis toujours, je n'avais jusqu'en 2019 pas connaissance
                    du métier de développeur web, suite à une innitiation, j'avais beaucoup de difficultés à quitter mon
                    ordinateur, au point même de me dire "wow on est vraiment payé à faire ce métier ?" sa me parrait fou
                    car je n'ai jamais exercé avec autant de plaisirs. J'ai beaucoup à apprendre, et je le ferait avec
                    toutes la passion que j'éprouve à me lever chaque matin pour faire un métier que j'aime.
                    </div>
                </div>
            </div>


        </>
    )
        ;

}

export default Moi;