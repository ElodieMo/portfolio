import './Competences.css';

function Competences() {
    return (
        <>
            <div className="competences" id="competences">Compétences</div>
            <div className="competences1">
                <div className="pro">Professionnelles
                    <div className="un">HTML</div>
                    <div className="un">CSS</div>
                    <div className="un">REACT</div>
                    <div className="un">PHP</div>
                    <div className="un">SQL</div>
                    <div className="un">LARAVEL</div>
                    <div className="un">JAVA</div>
                    <div className="un">SYMFONY</div>
                    <div className="un">SPRING</div>
                    <div className="un">JAVASCRIPT</div>
                    <div className="un">GITLAB</div>
                    <div className="un">METHODE AGILE</div>
                </div>
                <div className="skills">Soft Skills
                    <div className="un">PATIENTE</div>
                    <div className="un">CURIEUSE</div>
                    <div className="un">RIGOUREUSE</div>
                    <div className="un">IMPLIQUEE</div>
                    <div className="un">COURAGEUSE</div>
                    <div className="un">A L'ECOUTE</div>
                    <div className="un">AUTONOME</div>
                    <div className="un">ESPRIT D'EQUIPE</div>
                    <div className="un">PONCTUELLE</div>
                    <div className="un">PERSEVERANTE</div>
                    <div className="un">VOLONTAIRE</div>
                    <div className="un">INVESTIE</div>
                </div>
            </div>
        </>
    );
}

export default Competences;